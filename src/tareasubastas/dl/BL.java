package tareasubastas.dl;
import tareasubastas.bl.entities.administrador.Administrador;
import tareasubastas.bl.entities.articulo.Articulo;
import tareasubastas.bl.entities.coleccionista.Coleccionista;
import tareasubastas.bl.entities.puja.Puja;
import tareasubastas.bl.entities.subasta.Subasta;
import tareasubastas.bl.entities.usuario.Usuario;
import tareasubastas.bl.entities.vendedor.Vendedor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author Brian Alonso Baca
 */
public class BL
{
    /**
     * Los Usuarios.
     */
    public ArrayList<Usuario> usuarios; //comprobar si existe el usuario
    /**
     * Las Subastas.
     */
    public ArrayList<Subasta> subastas;


    /**
     * Intancia para creas los ArrayList
     */
    public BL()
    {
        this.usuarios = new ArrayList<Usuario>();
        this.subastas = new ArrayList<Subasta>();
    }

    /**
     * Registrar persona.
     *
     * @param u the u
     */
    public void registrarPersona(Usuario u){
        if(usuarios == null){
            usuarios = new ArrayList<>();
        }
        usuarios.add(u);
    }


    /**
     * Registrar vendedor
     *
     * @param nombre    El nombre del vendedor
     * @param direccion La direccion del vendedor
     * @param email     El email del vendedor
     * @param tipo      El tipo de vendedor
     * @return the string retorna un mensaje indicando que se logro registrar el vendedor
     */
    public String registrarVendedor(String nombre, String direccion, String email, int tipo){
        Vendedor vend = new Vendedor(email,tipo,nombre,direccion);
        registrarPersona(vend);
        return "Usuario registrado con éxito";
    }

    /**
     * Registrar coleccionista en el sistema
     *
     * @param nombre     El nombre del coleccionista
     * @param apellido   El apellido del coleccionista
     * @param id         El id del coleccionista
     * @param contrasena La contrasena del coleccionista
     * @param edad       La edad del coleccionista
     * @param estado     El estado del coleccionista
     * @param email      El  email del coleccionista
     * @param puntuacion La puntuacion del coleccionista en el sistema
     * @param tipo       El tipo del coleccionista como usuario
     * @param direccion  La direccion del coleccionista
     * @return the string
     */
    public String registrarColeccionista(String nombre, String apellido, String id,String contrasena, int year,int mes,int dia, int edad,String estado,String email,String puntuacion, int tipo,String direccion){
        LocalDate fecha = LocalDate.of(year,mes,dia);
        Coleccionista colec = new Coleccionista(email,tipo,nombre,apellido,id,fecha,edad,estado,puntuacion,direccion,contrasena);
        registrarPersona(colec);
        return "Usuario registrado con éxito";
    }

    /**
     * Registrar administrador en el sistema
     *
     * @param nombre     El nombre del administrador
     * @param apellido   El apellido del administrador
     * @param id         El id del administrador
     * @param contrasena La contrasena del administrador
     * @param edad       La edad del administrador
     * @param estado     El estado del administrador
     * @param email      El email del administrador
     * @param tipo       El tipo del administrador en el sistema
     * @return the string
     */
    public String registrarAdministrador(String nombre, String apellido, String id,String contrasena, int year,int mes,int dia, int edad,String estado,String email, int tipo){
        LocalDate fecha = LocalDate.of(year,mes,dia);
        Administrador admin = new Administrador(email,tipo,nombre,apellido,id,fecha,edad,estado,contrasena);
        registrarPersona(admin);
        return "Usuario registrado con éxito";
    }


    /**
     * Buscar usuario en el sistema, para confirmar el tipo de usuario y si existe
     *
     * @param nombre El nombre del usuario
     * @param email  El email del usuario
     * @return the int que es el tipo de usuario
     */
    public int buscarUsuario(String nombre,String email)
    {
        int tipo = 3;
        for (Iterator<Usuario> it = usuarios.iterator(); it.hasNext();)
        {
            Usuario u = it.next();
            if(u.getNombre().equals(nombre) && u.getEmail().equals(email))
            {
                if(u.getTipo()==0)
                {
                    tipo=0;
                }
                else if(u.getTipo()==1)
                {
                    tipo=1;
                }
                else if(u.getTipo()==2)
                {
                    tipo=2;
                }
            }
        }
        return tipo;
    }

    /**
     * Agregar subasta.
     * funcion para agregar las subastas
     *
     */
    public void agregarSubasta(String nombre, String email, Subasta s)
    {
        int tipo = 2;
        for (Iterator<Usuario> it = usuarios.iterator(); it.hasNext();)
        {
            Usuario u = it.next();
            if(u.getNombre().equals(nombre) && u.getEmail().equals(email))
            {
                u.addSubasta(s);
                subastas.add(s);
            }
        }
    }

    /**
     * Agregar puja usuario.
     * funcion para las pujas de los usuarios coleccionistas
     */
    public void agregarPujaUsuario(String nombre, String email, Puja p)
    {
        for (Iterator<Usuario> it = usuarios.iterator(); it.hasNext();)
        {
            Usuario u = it.next();
            if(u.getNombre().equals(nombre) && u.getEmail().equals(email))
            {
                u.addPuja(p);
            }
        }
    }

    /**
     * Agregar puja subasta boolean.
     *
     * @param codigo el codigo de la subasta
     * @return the boolean que retorna true si se realizo la puja
     */
    public boolean agregarPujaSubasta(int codigo, Puja p)
    {
        boolean LoEncontro=false;
        for (Iterator<Subasta> it = subastas.iterator(); it.hasNext();)
        {
            Subasta s = it.next();
            if(s.getCodigosubasta()==codigo)
            {
                s.addPuja(p);
                LoEncontro=true;
            }
        }
        return LoEncontro;
    }

    /**
     * Registrar articulo en el sistema
     *
     * @param codigo         el codigo de la subasta en la cual se encuentra el articulo
     * @param nombreArticulo el nombre del articulo
     * @param descripcion    la descripcion del articulo
     * @param estado         el estado del articulo
     * @param valor          el valor del articulo
     * @param categoria      la categoria del articulo
     * @param fechainicio    la fechainicio del articulo
     * @return the string
     */
    public String registrarArticulo(int codigo, String nombreArticulo, String descripcion, String estado, int dia, int mes, int year, int valor, String categoria, String fechainicio, String nombre, String email) {
        LocalDate fechaAntiguedad = LocalDate.of(year,mes,dia);
        Articulo a = new Articulo(nombreArticulo,descripcion,estado,fechaAntiguedad,categoria,valor,fechainicio);
        Subasta subasta = new Subasta(codigo, a);
        agregarSubasta(nombre, email, subasta);
        return "Articulo registrado con éxito";
    }


    /**
     * Eliminar subasta del sistema .
     *
     * @param codigo el codigo de la subasta
     * @return the boolean que determina si se logro la accion
     */
    public boolean eliminarSubasta(int codigo)
    {
        boolean LoEncontro=false;
        Subasta s = subastas.stream().filter(x -> codigo==x.getCodigosubasta()).findFirst().orElse(null);
        int subastaCount = subastas.size();
        subastas.remove(s);

        if (subastaCount != subastas.size()){
            LoEncontro = true;
        }

        return LoEncontro;
    }
}




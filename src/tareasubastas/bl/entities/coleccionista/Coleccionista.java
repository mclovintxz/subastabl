package tareasubastas.bl.entities.coleccionista;

import tareasubastas.bl.entities.usuario.Usuario;

import java.time.LocalDate;

/**
 * @author Brian Alonso Baca
 */

public class Coleccionista extends Usuario {
    private String apellido;
    private String id;
    private LocalDate fechaNacimiento;
    private int edad;
    private String estado;
    private String puntuacion;
    private String direccion;
    private String contrasena;

    /**
     * Constructor por defecto
     */
    public Coleccionista (){
    }


    /**
     * Instantiates para Coleccionista
     *
     * @param email           el email del coleccionista
     * @param tipo            el tipo del coleccionista
     * @param nombre          el nombre del coleccionista
     * @param apellido        el apellido del coleccionista
     * @param id              el id del coleccionista
     * @param fechaNacimiento la fecha nacimiento del coleccionista
     * @param edad            la edad del coleccionista
     * @param estado          el estado del coleccionista
     * @param puntuacion      la puntuacion del coleccionista
     * @param direccion       la direccion del coleccionista
     * @param contrasena      la contrasena del coleccionista
     */
    public Coleccionista(String email, int tipo, String nombre, String apellido, String id, LocalDate fechaNacimiento, int edad, String estado, String puntuacion, String direccion, String contrasena) {
        super(email, tipo, nombre);
        this.nombre = nombre;
        this.apellido = apellido;
        this.id = id;
        this.fechaNacimiento = fechaNacimiento;
        this.edad = edad;
        this.estado = estado;
        this.puntuacion = puntuacion;
        this.direccion = direccion;
        this.contrasena = contrasena;
    }

    /**
     * Gets del apellido.
     *
     * @return el apellido
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * Sets del apellido.
     *
     * @param apellido el apellido
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * Gets del id.
     *
     * @return el id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets del id.
     *
     * @param id el id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets de la fecha nacimiento.
     *
     * @return la fecha nacimiento
     */
    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * Sets de la fecha nacimiento.
     *
     * @param fechaNacimiento la fecha nacimiento
     */
    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    /**
     * Gets de la edad.
     *
     * @return la edad
     */
    public int getEdad() {
        return edad;
    }

    /**
     * Sets de la edad.
     *
     * @param edad la edad
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * Gets del  estado.
     *
     * @return el estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Sets del estado.
     *
     * @param estado el estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Gets para puntuacion.
     *
     * @return la puntuacion
     */
    public String getPuntuacion() {
        return puntuacion;
    }

    /**
     * Sets para la puntuacion.
     *
     * @param puntuacion la puntuacion
     */
    public void setPuntuacion(String puntuacion) {
        this.puntuacion = puntuacion;
    }

    /**
     * Gets para la direccion.
     *
     * @return la direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Sets para la direccion.
     *
     * @param direccion para la  direccion
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Gets para la contrasena.
     *
     * @return la contrasena
     */
    public String getContrasena() {
        return contrasena;
    }

    /**
     * Sets para la  contrasena.
     *
     * @param contrasena la contrasena
     */
    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }


    @Override
    public String toString() {
        return "Coleccionista{" +
                "apellido='" + apellido + '\'' +
                ", id='" + id + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", edad=" + edad +
                ", estado='" + estado + '\'' +
                ", puntuacion='" + puntuacion + '\'' +
                ", direccion='" + direccion + '\'' +
                ", contrasena='" + contrasena + '\'' +
                ", nombre='" + nombre + '\'' +
                ", email='" + email + '\'' +
                ", tipo=" + tipo +
                '}';
    }
}

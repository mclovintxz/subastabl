package tareasubastas.bl.entities.administrador;

import tareasubastas.bl.entities.usuario.Usuario;

import java.time.LocalDate;

/**
 * @author Brian Alonso Baca
 */

public class Administrador extends Usuario {
    private  String apellido;
    private  String id;
    private LocalDate fechaNacimiento;
    private  int edad;
    private  String estado;
    private String contrasena;

    /**
     * Constructor por defecto
     */
    public Administrador (){
    }

    /**
     * Instantiates para Administrador
     *
     * @param email           el email del administrador
     * @param tipo            el tipo del administrador
     * @param nombre          el nombre del administrador
     * @param apellido        el apellido del administrador
     * @param id              el id del administrador
     * @param fechaNacimiento la fecha nacimiento del administrador
     * @param edad            la edad del administrador
     * @param estado          el estado del administrador
     * @param contrasena      la contrasena del administrador
     */
    public Administrador(String email, int tipo, String nombre, String apellido, String id, LocalDate fechaNacimiento, int edad, String estado, String contrasena) {
        super(email, tipo,nombre);
        this.contrasena = contrasena;
        this.nombre = nombre;
        this.apellido = apellido;
        this.id = id;
        this.fechaNacimiento = fechaNacimiento;
        this.edad = edad;
        this.estado = estado;
    }

    /**
     * Geter para el apellido.
     *
     * @return el apellido
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * set para el  apellido.
     *
     * @param apellido el apellido
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * Geter para el id
     *
     * @return el id
     */
    public String getId() {
        return id;
    }

    /**
     * Set para id.
     *
     * @param id el id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets fecha nacimiento.
     *
     * @return la fecha nacimiento
     */
    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * Set de la  fecha nacimiento.
     *
     * @param fechaNacimiento la fecha nacimiento
     */
    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    /**
     * Geter de la  edad.
     *
     * @return la edad
     */
    public int getEdad() {
        return edad;
    }

    /**
     * Set de la  edad.
     *
     * @param edad la edad
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * Geter del  estado.
     *
     * @return el estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * set del estado.
     *
     * @param estado el estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Geter contrasena.
     *
     * @return la contrasena
     */
    public String getContrasena() {
        return contrasena;
    }

    /**
     * Sets de la contrasena.
     *
     * @param contrasena la contrasena
     */
    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    @Override
    public String toString() {
        return "Administrador{" +
                "apellido='" + apellido + '\'' +
                ", id='" + id + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", edad=" + edad +
                ", estado='" + estado + '\'' +
                ", contrasena='" + contrasena + '\'' +
                ", nombre='" + nombre + '\'' +
                ", email='" + email + '\'' +
                ", tipo=" + tipo +
                '}';
    }
}

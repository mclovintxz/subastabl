package tareasubastas.bl.entities.subasta;
import tareasubastas.bl.entities.articulo.Articulo;
import tareasubastas.bl.entities.puja.Puja;

import java.util.ArrayList;

/**
 * @author Brian Alonso Baca
 */
public class Subasta
{
    private int codigosubasta;
    private Articulo articulo;
    private ArrayList<Puja> pujas;
    //existirá un  valor por artículo y por el total
    //total = suma de todos los valores de c/u de los artículos

    /**
     * Constructor por defecto
     */
    public Subasta (){

    }

    /**
     * Instantiates para nueva Subasta
     *
     * @param codigo   the codigo
     * @param articulo the articulo
     */
    public Subasta(int codigo, Articulo articulo)
    {
        this.codigosubasta = codigo;
        this.articulo = articulo;
        this.pujas = new ArrayList<Puja>();
    }

    /**
     * Gets codigosubasta.
     *
     * @return the codigosubasta
     */
    public int getCodigosubasta()
    {
        return codigosubasta;
    }

    /**
     * Sets codigosubasta.
     *
     * @param codigosubasta the codigosubasta
     */
    public void setCodigosubasta(int codigosubasta)
    {
        this.codigosubasta = codigosubasta;
    }

    /**
     * Gets articulo.
     *
     * @return the articulo
     */
    public Articulo getArticulo()
    {
        return articulo;
    }

    /**
     * Sets articulo.
     *
     * @param articulo the articulo
     */
    public void setArticulo(Articulo articulo)
    {
        this.articulo = articulo;
    }

    /**
     * Add puja.
     *
     * @param p the p
     */
    public void addPuja(Puja p)
    {
        pujas.add(p);
    }

    /**
     * Gets pujas.
     *
     * @return the pujas
     */
    public ArrayList<Puja> getPujas()
    {
        return pujas;
    }

    /**
     * Sets pujas.
     *
     * @param pujas the pujas
     */
    public void setPujas(ArrayList<Puja> pujas)
    {
        this.pujas = pujas;
    }


}


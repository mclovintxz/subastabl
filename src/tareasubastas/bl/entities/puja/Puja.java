package tareasubastas.bl.entities.puja;

/**
 * @author Brian Alonso Baca
 */

public class Puja
{
    private String nombrecomprador;
    private int valorpuja;
    private int codigosubasta;

    /**
     * Constructor por defecto
     */
    public Puja (){

    }

    /**
     * Instantiates para una Puja
     *
     * @param nombre the nombre
     * @param valor  the valor
     * @param codigo the codigo
     */
    public Puja(String nombre, int valor, int codigo)
    {
        this.nombrecomprador = nombre;
        this.valorpuja = valor;
        this.codigosubasta = codigo;
    }

    /**
     * Gets para el nombrecomprador.
     *
     * @return el nombrecomprador
     */
    public String getNombrecomprador()
    {
        return nombrecomprador;
    }

    /**
     * Sets para el nombrecomprador.
     *
     * @param nombrecomprador nombre nombrecomprador
     */
    public void setNombrecomprador(String nombrecomprador)
    {
        this.nombrecomprador = nombrecomprador;
    }

    /**
     * Gets del valorpuja.
     *
     * @return el valorpuja
     */
    public int getValorpuja()
    {
        return valorpuja;
    }

    /**
     * Sets para valorpuja.
     *
     * @param valorpuja el valorpuja
     */
    public void setValorpuja(int valorpuja)
    {
        this.valorpuja = valorpuja;
    }

    /**
     * Gets para el  codigosubasta.
     *
     * @return el codigosubasta
     */
    public int getCodigosubasta()
    {
        return codigosubasta;
    }

    /**
     * Sets para el codigosubasta.
     *
     * @param codigosubasta el codigosubasta
     */
    public void setCodigosubasta(int codigosubasta)
    {
        this.codigosubasta = codigosubasta;
    }

    @Override
    public String toString() {
        return "Puja{" +
                "nombrecomprador='" + nombrecomprador + '\'' +
                ", valorpuja=" + valorpuja +
                ", codigosubasta=" + codigosubasta +
                '}';
    }
}



package tareasubastas.bl.logic;
import tareasubastas.bl.entities.vendedor.Vendedor;
import tareasubastas.dl.BL;

public class Gestor {
    private BL logica;

    public Gestor (){
        logica = new BL();
    }

    public String registrarVendedor(String nombre, String direccion, String email, int tipo){
        Vendedor vend = new Vendedor(email,tipo,nombre,direccion);
        logica.registrarPersona(vend);
        return "Usuario registrado con éxito";
    }

}
